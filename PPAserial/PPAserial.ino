/***************************************************
Multi-Stepper controller for a parallel prismatic actuator
 ****************************************************/


#include <AccelStepper.h>
#include <MultiStepper.h>

// Define some steppers and the pins the will use
AccelStepper stepperBot(AccelStepper::DRIVER, 11,10); // Format is: (AccelStepper::DRIVER, STEP,DIR);pin1 is step, pin2 is dir. Can also add an enable pin if required
AccelStepper stepperTop(AccelStepper::DRIVER, 9,8);
MultiStepper steppers;

// int32_t topPos = 0;
// int32_t BotPos = 0;
float mh = 1206.5; // Max Hypoteneuse (point to end effector distance)
float bl = 41.5*25.4; // Base Length (distance between two posts)
float h = sqrt(mh*mh - bl*bl);
float l1 = mh;

float l2 = mh;

float positions[2];
int32_t stepPositions[2];

bool newMove = 0;

String readStr = "";

void setup() {
    Serial.begin(9600);
    while (!Serial) delay(1); // wait for Serial on Leonardo/Zero, etc
    stepperTop.setMaxSpeed(10000.0);
    stepperTop.setAcceleration(1.0);
    stepperTop.setCurrentPosition((int32_t)mh*160);
    stepperTop.moveTo(stepperTop.currentPosition());
    steppers.addStepper(stepperTop);
    
    stepperBot.setMaxSpeed(10000.0);
    stepperBot.setAcceleration(1.0);  
    stepperBot.setCurrentPosition((int32_t)mh*160);
    stepperBot.moveTo(stepperBot.currentPosition());
    steppers.addStepper(stepperBot);

    stepPositions[0] = (int32_t)mh*160;
    stepPositions[1] = (int32_t)mh*160;
    
}

void loop(){
    while (Serial.available() > 0) {
      char c = Serial.read();
      readStr += c;
      if (c == '\n') {
        newMove = 1;
        break;
      }
    }

    if (newMove == 1) {
      int commaIndex = readStr.indexOf(',');
      String strX = readStr.substring(0, commaIndex);
      String strY = readStr.substring(commaIndex+1);

      positions[0] = strX.toFloat();
      positions[1] = strY.toFloat();
      positions[2] = 0;

      moveXY(positions[0],positions[1]);
      moveZ(positions[2]);

      Serial.println("DONE");
      readStr = "";
      newMove = 0;
    }
}

void moveXY(float xPos, float yPos){
    l1 = sqrt(xPos*xPos + yPos*yPos);
    l2 = sqrt((bl-xPos)*(bl-xPos) + yPos*yPos);
    stepPositions[0] = (int32_t)l1 * 160;
    stepPositions[1] = (int32_t)l2 * 160;
    steppers.moveTo(stepPositions);
    steppers.runSpeedToPosition(); // Use runSpeedToPosition to have blocking
}

void moveZ(float zPos){
    stepPositions[0] = (int32_t)zPos * 160;
    stepPositions[1] = (int32_t)zPos * 160;
    zSteppers.moveTo(stepPositions);
    zSteppers.runSpeedToPosition();
}
