# Parallel Prismatic Actuator (PPA)

<img src="images/ppaTable.gif" width="800">

A 3-axis motion platform designed to minimize moving mass and inertia. Prototyped with the CBA at Haystack in July of 2021. Two prismatic actuators form a 3-bar linkage with adjustable side-lengths, which allows for an addressable XY space formed by the intersection of the two arm's revolved maximum length.

Note that the forward kinematics ignore the axes rotary centers not being coincident with their linear motion (an offset of ~25mm). This discrepancy should be accounted for before the system is adapted into a project requiring a higher degree of accuracy.

The CAD presented here uses a GT2 timing belt with a fixed NEMA17 stepper mounted to a rotating Z-axis. This belt + motor shuttles a 5/8" carbon fiber tube through custom turned acetal bushings. Many of the design decisions were based on what was immediately available at Haystack / the lab before leaving, so there's plenty of room for optimization left.

Two scripts are provided, one which takes the actuator through a canned series of motions forming a rectangle, while the other listens for serial inputs from a MODs toolpath pipleine written by Eyal Perry. Click the picture below to find a video of the setup running.

[<img src="images/forestBeing.png" width="500">](https://gitlab.cba.mit.edu/davepreiss/parallel-prismatic-actuator/-/blob/master/images/forestBeing.mp4)

<img src="images/ppaCAD.png" width="700">
