/***************************************************
A canned routine to move in a ~rectangular path on the parallel prismatic actuator (PPA)
 ****************************************************/


#include <AccelStepper.h>
#include <MultiStepper.h>

// Define some steppers and the pins the will use
AccelStepper stepperBot(AccelStepper::DRIVER, 11,10); // Format is: (AccelStepper::DRIVER, STEP,DIR);pin1 is step, pin2 is dir. Can also add an enable pin if required
AccelStepper stepperTop(AccelStepper::DRIVER, 9,8);
MultiStepper steppers;

AccelStepper stepperZ1(AccelStepper::DRIVER, 7,6); // Format is: (AccelStepper::DRIVER, STEP,DIR);pin1 is step, pin2 is dir. Can also add an enable pin if required
AccelStepper stepperZ2(AccelStepper::DRIVER, 5,4);
MultiStepper zSteppers;

// int32_t topPos = 0;
// int32_t BotPos = 0;
float mh = 1206.5; // Max Hypoteneuse (point to end effector distance)
float bl = 1000.76; // Base Length (distance between two posts)
float h = sqrt(mh*mh - bl*bl);
float l1 = mh;
float l2 = mh;

//float positions[2];
int32_t stepPositions[2];

int a = 0;

void setup() {
    Serial.begin(9600);
    while (!Serial) delay(1); // wait for Serial on Leonardo/Zero, etc
    stepperTop.setMaxSpeed(10000.0);
    stepperTop.setAcceleration(1.0);
    stepperTop.setCurrentPosition((int32_t)mh*160);
    stepperTop.moveTo(stepperTop.currentPosition());
    steppers.addStepper(stepperTop);
    
    stepperBot.setMaxSpeed(10000.0);
    stepperBot.setAcceleration(1.0);  
    stepperBot.setCurrentPosition((int32_t)mh*160);
    stepperBot.moveTo(stepperBot.currentPosition());
    steppers.addStepper(stepperBot);

    stepperZ1.setMaxSpeed(5000.0);
    stepperZ1.setAcceleration(1.0);  
    stepperZ1.setCurrentPosition((int32_t)0);
    stepperBot.moveTo(stepperBot.currentPosition());
    zSteppers.addStepper(stepperZ1);

    stepperZ2.setMaxSpeed(5000.0);
    stepperZ2.setAcceleration(1.0);  
    stepperZ2.setCurrentPosition((int32_t)0);
    stepperZ2.moveTo(stepperBot.currentPosition());
    zSteppers.addStepper(stepperZ2);

    stepPositions[0] = (int32_t)mh*160;
    stepPositions[1] = (int32_t)mh*160;
}

void loop(){
    if(a == 0){              
        moveXY(500,300);
        moveXY(0,300);
        moveZ(-10);
        moveXY(0,600);
        moveXY(1000,600);
        moveXY(1000,300);
        moveZ(0);
        moveXY(bl/2,h+2); // return home
        a = 1;
     }
}

void moveXY(float xPos, float yPos){
    l1 = sqrt(xPos*xPos + yPos*yPos);
    l2 = sqrt((bl-xPos)*(bl-xPos) + yPos*yPos);
    stepPositions[0] = (int32_t)l1 * 160;
    stepPositions[1] = (int32_t)l2 * 160;
    steppers.moveTo(stepPositions);
    steppers.runSpeedToPosition(); // Use runSpeedToPosition to have blocking
}

void moveZ(float zPos){
    stepPositions[0] = (int32_t)zPos * 160;
    stepPositions[1] = (int32_t)zPos * 160;
    zSteppers.moveTo(stepPositions);
    zSteppers.runSpeedToPosition();
}
